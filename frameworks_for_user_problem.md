# Frameworks Overview
- to choose the right one - https://chatgpt.com/g/g-vZ7SgKBOh-framework-finder

## Business and Strategy
- **Porter's Five Forces**: Analyzes the competitive forces within an industry.
  - *Example*: Analyzing the news platform industry to understand competitive pressures.
- **SWOT Analysis**: Identifies strengths, weaknesses, opportunities, and threats.
  - *Example*: Evaluating a company's market position and strategic options.
- **PESTEL Analysis**: Analyzes external factors (Political, Economic, Social, Technological, Environmental, Legal).
  - *Example*: Assessing the impact of regulatory changes on the tech industry.
- **Ansoff Matrix**: Helps companies plan their product and market growth strategy.
  - *Example*: Developing a growth strategy for a new product line.
- **BCG Matrix**: Evaluates a company's product portfolio.
  - *Example*: Categorizing products into stars, cash cows, question marks, and dogs.
- **VRIO Framework**: Assesses resources and capabilities for competitive advantage.
  - *Example*: Evaluating a company's unique resources to sustain competitive advantage.
- **Business Model Canvas**: Outlines key components of a business model.
  - *Example*: Designing a business model for a startup.
- **Balanced Scorecard**: Measures organizational performance across different perspectives.
  - *Example*: Tracking performance in finance, customer satisfaction, internal processes, and learning.
- **Value Chain Analysis**: Identifies key activities that create value for the customer.
  - *Example*: Analyzing the steps in a company's production process to improve efficiency.
- **GE-McKinsey Matrix**: Evaluates business portfolio based on market attractiveness and competitive strength.
  - *Example*: Strategic planning for diversified companies.
- **Porter's Value Chain**: Framework for analyzing specific activities through which firms can create value.
  - *Example*: Identifying cost advantages in business processes.
- **McKinsey 7S Framework**: Analyzes organizational effectiveness.
  - *Example*: Aligning key organizational elements for improved performance.

## Problem Solving and Decision Making
- **Root Cause Analysis (RCA)**: Identifies the root causes of problems.
  - *Example*: Investigating the root cause of a manufacturing defect.
- **Fishbone Diagram (Ishikawa)**: Visualizes cause and effect relationships.
  - *Example*: Mapping out the potential causes of a delay in project delivery.
- **5 Whys**: A simple technique to drill down into the root cause of a problem.
  - *Example*: Asking "why" repeatedly to understand the underlying issue of a customer complaint.
- **Six Thinking Hats**: Encourages different perspectives in decision-making.
  - *Example*: Using different hats to explore a business problem from multiple viewpoints.
- **Decision Matrix (Eisenhower Matrix)**: Prioritizes tasks based on urgency and importance.
  - *Example*: Organizing tasks into four quadrants to focus on what's most important.
- **Kepner-Tregoe Method**: Systematic problem analysis and decision-making.
  - *Example*: Analyzing options and making a decision on a major business investment.
- **PDCA Cycle (Deming Cycle)**: Plan-Do-Check-Act for continuous improvement.
  - *Example*: Continuously improving processes through iterative cycles of planning, doing, checking, and acting.
- **SCAMPER Technique**: Idea generation through Substitution, Combination, Adaptation, Modification, Put to another use, Elimination, and Rearrangement.
  - *Example*: Generating new product ideas by modifying existing ones.

## Project Management
- **Gantt Chart**: Visualizes project schedule and timelines.
  - *Example*: Planning and tracking the timeline of a software development project.
- **PERT Chart**: Analyzes project tasks and timelines.
  - *Example*: Estimating the shortest path to complete a construction project.
- **Critical Path Method (CPM)**: Identifies the longest path of planned activities.
  - *Example*: Determining the critical path in a product launch timeline.
- **RACI Matrix**: Clarifies roles and responsibilities in a project.
  - *Example*: Defining who is Responsible, Accountable, Consulted, and Informed in a project team.
- **Agile Frameworks (Scrum, Kanban)**: Promotes iterative and flexible project management.
  - *Example*: Implementing Scrum for software development to enhance flexibility and speed.
- **PMI's PMBOK**: A guide to project management best practices.
  - *Example*: Applying PMBOK guidelines to standardize project management processes.
- **PRINCE2**: Structured project management method.
  - *Example*: Managing government and large-scale infrastructure projects.
- **Lean Project Management**: Focuses on delivering more value with less waste in a project context.
  - *Example*: Streamlining processes to improve project efficiency.

## Marketing and Sales
- **4Ps/7Ps of Marketing Mix**: Product, Price, Place, Promotion (+ People, Process, Physical Evidence).
  - *Example*: Developing a comprehensive marketing strategy for a new product.
- **AIDA Model**: Attention, Interest, Desire, Action - stages of customer journey.
  - *Example*: Crafting an advertising campaign to lead customers through the purchase funnel.
- **STP Model**: Segmentation, Targeting, Positioning.
  - *Example*: Identifying market segments and positioning a product to target them effectively.
- **Customer Journey Map**: Visualizes the customer experience.
  - *Example*: Mapping out the customer journey to identify touchpoints and improve customer satisfaction.
- **Brand Positioning Map**: Visualizes brand perception relative to competitors.
  - *Example*: Creating a brand positioning strategy to differentiate from competitors.
- **BCG Growth-Share Matrix**: Evaluates a company's product portfolio based on growth rate and market share.
  - *Example*: Allocating resources to business units based on their market growth and share.
- **Customer Lifetime Value (CLV)**: Measures the total value a customer brings over their entire relationship.
  - *Example*: Calculating the profitability of retaining customers over time.

## Finance and Economics
- **DuPont Analysis**: Breaks down Return on Equity into component parts.
  - *Example*: Analyzing the components driving a company's financial performance.
- **Break-even Analysis**: Determines the point at which total revenue equals total costs.
  - *Example*: Calculating the break-even point for a new business venture.
- **Porter's Diamond Model**: Explains national competitive advantage.
  - *Example*: Understanding why certain industries thrive in specific countries.
- **Economic Value Added (EVA)**: Measures a company's financial performance.
  - *Example*: Evaluating the financial value a company generates above its cost of capital.
- **Activity-Based Costing (ABC)**: Allocates overhead costs more precisely.
  - *Example*: Understanding the true cost of producing a product.
- **Financial Ratios**: Analyzes financial performance using various ratio categories.
  - *Example*: Assessing a company's liquidity, profitability, and solvency.
- **Z-Score**: Predicts the probability of a company going bankrupt.
  - *Example*: Evaluating financial health to foresee potential bankruptcy.

## Organizational Behavior and Development
- **McKinsey 7S Framework**: Analyzes organizational effectiveness.
  - *Example*: Aligning key organizational elements for improved performance.
- **Lewin's Change Management Model**: Unfreeze, Change, Refreeze.
  - *Example*: Implementing a change management process for organizational transformation.
- **Kotter's 8-Step Change Model**: Steps for successful organizational change.
  - *Example*: Following eight steps to manage and lead change in an organization.
- **ADKAR Model**: Framework for change management (Awareness, Desire, Knowledge, Ability, Reinforcement).
  - *Example*: Guiding employees through the stages of change.
- **Tuckman's Stages of Group Development**: Forming, Storming, Norming, Performing, Adjourning.
  - *Example*: Understanding team dynamics and development stages.

## Personal Development and Psychology
- **SMART Goals**: Specific, Measurable, Achievable, Relevant, Time-bound goals.
  - *Example*: Setting clear and achievable personal development goals.
- **Maslow's Hierarchy of Needs**: Levels of human needs from basic to self-actualization.
  - *Example*: Ensuring basic needs are met to focus on higher levels of personal growth.
- **3 Ps of Positive Psychology**: Purpose, Persistence, Positive Thinking.
  - *Example*: Fostering a positive mindset and purpose-driven life.
- **Johari Window**: A tool for understanding interpersonal communication and relationships.
  - *Example*: Using feedback to improve self-awareness and interpersonal relationships.
- **Wheel of Life**: Assesses balance in various life areas to identify areas for improvement.
  - *Example*: Evaluating personal satisfaction in areas such as health, career, relationships, and spirituality.
- **Emotional Intelligence (EQ)**: Understanding and managing your own emotions, and recognizing and influencing others.
  - *Example*: Developing emotional intelligence to improve personal and professional relationships.
- **GROW Model**: Goal, Reality, Options, Will - coaching model for goal setting and problem-solving.
  - *Example*: Coaching individuals to achieve their personal and professional goals.

## Innovation and Creativity
- **SCAMPER Technique**: Idea generation through Substitution, Combination, Adaptation, Modification, Put to another use, Elimination, and Rearrangement.
  - *Example*: Generating new product ideas by modifying existing ones.
- **TRIZ (Theory of Inventive Problem Solving)**: A methodology for systematic innovation.
  - *Example*: Applying inventive principles to solve complex engineering problems.
- **Blue Ocean Strategy**: Creating uncontested market space and making competition irrelevant.
  - *Example*: Developing a new market niche with a unique product offering.
- **Design Thinking**: A human-centered approach to innovation.
  - *Example*: Using empathy and iterative prototyping to create user-centric products.
- **Lean Innovation**: Combines lean principles with innovation processes to reduce waste and increase value.
  - *Example*: Developing new products efficiently by validating ideas early and often.

## Operations and Quality Management
- **Lean Six Sigma**: A methodology that relies on collaborative team efforts to improve performance by systematically removing waste.
  - *Example*: Improving operational efficiency by reducing waste and defects.
- **Kaizen**: Continuous improvement process.
  - *Example*: Implementing small, incremental changes to improve quality and efficiency.
- **PDCA Cycle (Deming Cycle)**: Plan-Do-Check-Act for continuous improvement.
  - *Example*: Continuously improving processes through iterative cycles of planning, doing, checking, and acting.
- **Theory of Constraints (TOC)**: Identifies the most important limiting factor in a process and systematically improves it.
  - *Example*: Optimizing a production process by addressing the bottleneck that limits throughput.
- **Total Quality Management (TQM)**: Focuses on long-term success through customer satisfaction.
  - *Example*: Implementing quality management practices to enhance customer satisfaction and business performance.
- **ISO 9001**: International standard for quality management systems.
  - *Example*: Achieving ISO 9001 certification to demonstrate commitment to quality.
- **Six Sigma DMAIC**: Define, Measure, Analyze, Improve, Control - structured approach for process improvement.
  - *Example*: Reducing defects in manufacturing through a structured Six Sigma process.

## Other Frameworks
- **Balanced Scorecard**: A strategy performance management tool.
  - *Example*: Monitoring organizational performance across multiple perspectives such as financial, customer, internal processes, and learning & growth.
- **OKR (Objectives and Key Results)**: Framework for setting and tracking objectives and their outcomes.
  - *Example*: Setting ambitious goals and measurable results to drive organizational performance.
- **Pestel Analysis**: Analyzes the macro-environmental factors affecting an organization.
  - *Example*: Assessing the impact of external factors like political, economic, social, technological, environmental, and legal on a business.
- **PEST Analysis**: Analyzes Political, Economic, Social, and Technological factors.
  - *Example*: Evaluating the external environment to inform strategic planning.
- **Porter’s Generic Strategies**: Framework for achieving competitive advantage.
  - *Example*: Choosing between cost leadership, differentiation, or focus to compete in the market.
- **Value Proposition Canvas**: Tool for designing products and services that customers want.
  - *Example*: Identifying customer needs and creating value propositions that meet those needs.
- **Business Model Canvas**: A strategic management template for developing new business models.
  - *Example*: Outlining the key components of a business model for a startup.
- **VRIO Framework**: Evaluates resources to determine their competitive potential.
  - *Example*: Assessing a company's internal resources to find sustained competitive advantage.
- **SOAR Analysis**: Strengths, Opportunities, Aspirations, Results - a strategic planning tool.
  - *Example*: Creating a positive approach to strategic planning by focusing on strengths and opportunities.
- **MOST Analysis**: Mission, Objectives, Strategies, Tactics - a planning framework.
  - *Example*: Aligning an organization’s mission with its objectives, strategies, and tactics for effective execution.

This list covers a wide range of frameworks across various domains. While extensive, it may not be exhaustive as new frameworks are continually developed and adapted. If you need more details or additional frameworks, feel free to ask!  
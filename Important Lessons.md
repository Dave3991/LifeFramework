# Important Lessons

## 1. Important people come and go, and that's okay.

Unfortunately, the most important people in your life can become strangers overnight. Fortunately, total strangers can become the most important people in your life overnight. This process hurts, but if accepted, it serves to improve the quality and suitability of the people in your life.

## 2. Your diet isn't just what you eat.

As you get older, you realize that your diet isn't just what you eat; it's what you watch, what you read, who you follow, and who you spend your time with. So if your goal is to have a healthier mind, you have to start by removing all the junk from your diet.

## 3. You have to let people down to be happy.

You and your mental health are more important than your career, more money, other people's opinions, that event you said you would attend, your partner's mood, and your family's wishes. If taking care of yourself means letting someone down, then let someone down. Your self-love must always be stronger than your desire to be loved by others.

## 4. Never let rejection lead to self-rejection.

A person who has experienced rejection fears rejection, and a person that fears rejection tends to push or run away before they can be rejected. In their subconscious mind, they have avoided rejection. In reality, they've been rejected again, this time by themselves.

## 5. Own your responsibilities, own your future.

You're not responsible for your trauma, but you are responsible for breaking the cycle and not hurting more people because of what happened to you. You will never control your future if you let your present be controlled by your past. What happened yesterday may not be your responsibility, but how you behave today is.

## 6. Quality over quantity.

Life is about quality, not quantity. One quality friend gives you more than 100 acquaintances. One quality relationship gives you more than 100 flings. One quality experience gives you more than 100 drunken nights.

## 7. Fairytales will make you unhappy.

Obsessing over the things that society says you're "supposed to do" will kill your happiness. Don't listen to the fake fairytales of how your life is supposed to be going. You don't have to go to university at 18, get a job at 21, buy a house at 25, get married at 30, or have kids at 35. Everyone is different, and your path to happiness will be too.

## 8. Fun is yours.

If you want to enjoy your life, don't subscribe to other people's definition of "fun." Fun doesn't have to mean drinking, partying, and socializing. Fun can be a night alone, getting lost in a book, a deep conversation, a walk, creating art, playing music, or doing work that you love. Your fun belongs to you; make sure you define it.
# I turned 72 today

Here's 32 things I've learned that I hope help you in your journey:

1. It's usually better to be nice than right.
2. Nothing worthwhile comes easy.
3. Work on a passion project, even just 30 minutes a day. It compounds.
4. Become a lifelong learner (best tip).
5. Working from 7am to 7pm isn't productivity. It's guilt.
6. To be really successful become useful.
7. Like houses in need of repair, problems usually don’t fix themselves.
8. Envy is like drinking poison expecting the other person to die.
9. Don’t hold onto your “great idea” until it’s too late.
10. People aren’t thinking about you as much as you think.
11. Being grateful is a cheat sheet for happiness. (Especially today.)
12. Write your life plan with a pencil that has an eraser.
13. Choose your own path or someone will choose it for you.
14. Never say, I’ll never...
15. Not all advice is created equal.
16. Be the first one to smile.
17. The expense of something special is forgotten quickly. The experience lasts a lifetime. Do it.
18. Don’t say something to yourself that you wouldn’t say to someone else.
19. It’s not how much money you make. It’s how much you take home.
20. Feeling good is better than that “third” slice of pizza.
21. Who you become is more important than what you accomplish.
22. Nobody gets to their death bed and says, I’m sorry for trying so many things.
23. There are always going to be obstacles in your life. Especially if you go after big things.
24. The emptiest head rattles the loudest.
25. If you don’t let some things go, they eat you alive.
26. Try to spend 12 minutes a day in quiet reflection, meditation, or prayer.
27. Try new things. If it doesn’t work out, stop. At least you tried.
28. NEVER criticize, blame, or complain.
29. You can’t control everything. Focus on what you can control.
30. If you think you have it tough, look around.
31. It’s only over when you say it is.
32. One hand washes the other and together they get clean. Help someone else.

If you're lucky enough to get up to my age, the view becomes more clear. It may seem like nothing good is happening to you, or just the opposite. Both will probably change over time.

source: [https://www.reddit.com/r/lifehacks/comments/1bgw44k/i_turned_72_today/](https://www.reddit.com/r/lifehacks/comments/1bgw44k/i_turned_72_today/)
# LifeFramework
### Foreword
This is guideline, which I try to live by. Original can be found here https://medium.com/@synopsi/what-i-ve-learned-in-my-30-years-7d5760b6d6a5

I'm open to add another points or modify some, just open merge request or issue :)

### Guideline
1. **Whoever you want to be, surround yourself with people that are closest to the type of person you’d like to be.** 
    - As Jim Rohn pointed out, “you are the average of the five people you spend the most time with.” 
    - You can influence yourself also with books
    - the most significant factor in any person's life is the people
        - write paper with people and mark them +/- if they inspire/energize you or not so you can select right people to be with

2. **Judge slowly, forgive quickly.** 
    - It’s very easy to be judgmental or burn bridges with people that hurt you badly. Be positive and give others second chances. The strongest relationships I’ve ever built started many times as a nitghtmares. 
    - If you get into conflict solve it, talk about it with that person, communication is the key to success

3. **Never, ever sacrifice your happiness.** 
    - No matter what decision are you facing, always consider its impact on your happiness. Nothing else matters, especially not the opinions of others.

4. **Freedom is the most important asset you’ll ever have.**

5. **Don’t hold onto things—they’ll only take away your freedom.**

6. **Money can’t buy happiness, but it can buy freedom.** 
    - As Jimmy Wales pointed out, keep your burn rate low. Make sure you take on as little debt as possible, but don’t restrain yourself from pleasures - especially traveling, learning new skills or gaining new experiences.

7. **Don’t follow crowds/fashion. Make your own opinions. Read more long form content, especially books.** 
    - Critical thinking
        - Aks questions
            - How does it feel to you ?
            - Is it right ?
            - And then what ?
            - even ask question about questions!

8. **Listen to your inner-self.** 
Everyone has a gut feeling. Start listening to it more often. Your unconsciousness is right more often that you would think. 
    - If you are not sure (like 98% sure), that it is the right one, then it is not

9. **Challenge yourself regularly, don’t allow yourself to get bored.**
    - Be open to new things
    - learn everyday
    - invest in yourself
    - strive to be better version of yourself every day.

10. **Don’t waste your time on mediocre relationships.**
    - find someone who adds value to your life, who helps you grow
    - don't change for relationship, stay authentic - https://youtu.be/9B6g3MKcfno?t=5842
    - Partners are what makes us great.
        - That's why Mario has Luigi.
        - That's why Zelda has Link.

11. **Limit the distractions in your life. Automatize as much as possible.** 
    - Develop habits that helps to relieve your mind and leave some space for a creative thinking.

12. **Explore the world, travel and talk to people.**
    - this doesn't mean you have to travel the world, explore your country, explore what your people needs the most

13. **Help others anytime you can, not only when you feel like it and don't expect anything back!**

14. **Don’t worry about what others think.** 
    - Become the best yourself you’ll—someone you’ll love and respect. Never allow others to shape you into a person that you’ll hate.
    - It's ok not to fit in others people expections.

15. **Spend money on experiences, not things.**
    - Every thing should have its meaning and place.
    - Do I really need it ?
    - Would I buy it again if it breaks ?
    - Who are we buying it for ?
    - Will it benefit my life ?
    - Is this the best use of my money?
    - can my money be spent better to make me happier?

16. **Be more selfish and learn how to say no to people around you. Stop being a “yes sayer”.**
    - Stop worrying about pleasing others and focus on yourself first. People will enjoy your company more when you truly want to be around them, not when you’re fulfilling a sense of obligation.

17. **Be honest with yourself. Understand your strengths and weaknesses.** 
    - From weakness comes failure

18. **The key to success is adaptability and persistence.**
    - Learn new things quickly, and incorporate what you learn into what you do. Don’t stop doing something you love because others don’t believe in what you’re doing. Stick with it as long as you’re still passionate about it.

19. **Never hurt others consciously.**
    - Unhappines of your enemies won't help anything

20. **Replace hour-based work days to task-based work days.** 
    - The worst thing you can do is to spend 20 hours in the office, only to waste 18 of them on Facebook. Instead, work until you finish tasks that you’ve planned for the day. Read the 4-Hour Workweek.
    - How to get rich when you don't work ? 
    - Have sideproject
    - Ultimate goal is passive income

21. **It’s better to be sorry to do something that regret not trying it.** 
    - There are sins that it would be a shame not to do

22. **You should never consider money when considering taking on a new opportunity (job, startup).**
    - knowledge > money
 
23. **Don’t do something just for a sake of learning.** 
    - You learn a lot in a jail, but that doesn’t mean that you want to end up in one. You’ll learn a lot anywhere. Don’t make it the primary decision point. 

24. **It’s ok to be greedy. Don’t be afraid to ask for what you want.**

25. **Build stuff. There’s nothing like the sense of accomplishment you feel after building something.** 
    - Especially applicable on physical things that you can see or touch. Paint your room, plant new plants, build a small dam, or a tesla coil.

26. **You achieve more with a great smile on your face than banging your fist on a table.** 
    - Let's bring a smile wherever you go!

27. **Slow down to go further. Life is a marathon not a sprint.** 
    - Keep your FOMO on leash. 
 
28. **Success attracts hatred and envy. Ignore it.** 
    - You can't be an important and life-changing presence for some people without also being joke and an embarrassment to others

29. **Aim high. You are capable of more than you think.** 
    - “If you can dream it, you can do it.” 
    - Think big!
    - Set goals that scares you a little and excite you a lot!

30. **Workout regularly. Eat good food. Experiment.** 
    - “No man has the right to be an amateur in the matter of physical training. It is a shame for a man to grow old without seeing the beauty and strength of which his body is capable.”

31. **Feel your fear but do them anyway**
    - the more we are out of the comfort zone, the more we grow
    - how would you live if you weren't afraid?
        - what would change ?
        - what would you be doing ?
    
32. **Attract great coaches/teachers/mentors**
    - show you have potential

33. **Build a supportive team and environment around you**
    - people you admire
    - helping each other no-matter what
    - Compliment more than you complain
    - environment has more influence on you than you think
        - Write motivation quotes on walls
            - "Do More Things That Make You Forget to Check Your Phone"
        - Have paintings/posters/pictures that make you think
         
34. **Peole will remember how you made feel not what you did or said** - Maya Angelou
    - Quality is remebered long after price is forgotten
    - We have few people around us that admire us

35. **Respect the little things**
    - the fact that someone sweetens your tea is not a matter of course

36. **Friendship is more than money**
    - "The most valuable of all your possessions are your friends." - Herodotes
    - when you think back on the best moments of your life, were you alone?

37. **No one is going to save you. You are in charge of your own destiny**
    - You are going to die
        - How do you want them to talk about you at the funeral?
        - Who do you want to have there?
    - Do I deserve this once more?
        - No? -> change something!

38. **You are what you do.**
    - not what you think
    - not what you say
    - Don't force yourself to do something because you want to be that person
        - Want to be karate master, but hate training
    - Getting upset won't help anything
    - If you think, that things you are doing are worth repeating eternally -> they are

39. **People are developing based on feedback.**
    - tell them what they are doing right (specifically)
    - tell them in which areas they should improve (very specifically - what and how)
        -  Don't be personal, they work sucks, not themselves
    - Be open to criticism, especially constructive one
    - Emotions are biggest motivation for human actions 

40. **Ask your older self if it would thank you for what you are doing?**

41. **Public display of emotions is a weak man's characteristic**
    - don't cry in public (it's ok in private or with close friends)
    - don't show anger in public (go and scream at home)
    - don't shout at your wife/closed ones in public

42. **Write things down**
    - our brains are for having ideas, not holding them
    - write everyhing what resonates with you

43. **The more responsibility we accept, the more meaningful life begins to make**

44. **We should say yes, to whatever gives us meaning in our lives**

45. **Life is an adventure and challenge**
    - be positive is best decision, you can do every morning
    - the journey towards one's dreams is one of the two main sources of optimism

46. **When you want something you don't have, you have to do things you don't do**

47. **Your family is the most important business, you'll ever run**

48. **When our needs aren't being met, we tend to think about what's wrong with other people**
    - If at first you did not succeed, find out why.

1. Assume comfort in any interaction

Comfort makes people open up easily.

2. Pay attention to people’s feet when you approach them

When you approach a group of people whilst in a conversation, pay attention to their bodies. If they turn only their torsos and not their feet, it means they are in the middle of an important conversation and they don't want you to interrupt them.

3. Whenever you have an argument with someone, stand next to them and not in front of them

You won’t appear as much of a threat and they will eventually calm down.

4. Whenever you need a favor, open with “I need your help”

We don’t really enjoy the guilt we feel for not helping someone out.

5. If you want people to feel good, give them validation. Rephrase what they just told you

This will make them think that you are a good listener and that you are really interested in them.

6. If you want to get a positive response from someone, nod while you talk

People usually respond well to mimicking, so they will most probably nod back while you talk.

7. Want to see if someone is paying attention to what you are saying? Fold your arms

If the other person pays attention, they will most likely emulate your action.

8. Having trouble remembering names? Repeat the other person’s name during the conversation

Repeating helps you remember.

9. If you ask someone a question and they only partially answer, just wait. They will keep talking

If they finish their response without providing a full answer, just wait. Stay silent and retain eye contact.

10. People usually focus on the emotion and not on the subject

Make sure to be enthusiastic and to try and evoke emotions.

11. Confidence is way more important than knowledge

Confidence makes people feel secure around you.

12. Fake it till you make it

You are what you believe you are.

13. If you want to be persuasive, try and reduce the use of the words “I think” and “I believe”

These words do not convey confidence.

14. A clean and organized environment affects your mood and productivity

You feel revitalized and calmer.

15. Want to find out which people are close to each other within a group? Pay attention to the people who look at each other when everyone in the group laughs at a joke.

People will instinctively look at the person they feel closest to within the group.

16. When you call a person you want to meet, show excitement

Excitement is contagious.

17. Want to build rapport and gain respect? Match body language

If you want to get someone’s attention, the best thing to do, when you approach them, is to match their body language.

18. When someone insults you, either ignore them or mock them. Never lose temper. Always control the frame

Arguing is a waste of time, but if you want to argue never lose your temper.

19. Stand up straight, have warm hands and always keep eye contact

These are all demonstrations of confidence.

20. The Benjamin Franklin effect

A person who has done someone a favor is more likely to do that person another favor than they would be if they had received a favor from that person.

source: https://www.reddit.com/r/socialskills/comments/gxr3h0/20_psychological_life_hacks_that_can_help_you/

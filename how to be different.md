## Stop consuming what everyone else consumes.
- Unfollow big accounts.
- Leave your phone at home.
- Don't watch news.

Let your mind wander to random streams of consciousness, to quiet corners of bookstores, to visions you wished existed.

## Iterate violently.
This is a game of experimentation disguised as consistency.\
Try something new until your audience likes sharing it, and you like making it.\
Then, hammer it relentlessly, until it's time to experiment again.

## What's the one sentence summary of what you create?
How do fans tell their friends about you?
How do you make every _word_ of that sentence different from what's out there?

## Go back further.
- Watch old movies.
- Read old books.
- Explore mediums that barely exist today- magazines, VHS tapes.

When you tap into content outside your 'for-you' feed, an entire world opens up. 
Different output starts with different input.

## Turn off your mind.
You will see things 25 minutes into a sauna session you can't see otherwise.
- Go for a long walk.
- Paddle to the middle of a lake.

Voices will come. Paths will appear. Truths will be deafening.

## Play.
- Time travel is real.
- Play a sport
- Play a game.
- Play anything- that makes you feel like a kid again.

Tapping into that version of yourself makes life simple.
Your stress will seem ridiculous. Your passion will feel obvious.

## Pay attention.
What do you see that no one else sees? 
- A word.
- A feeling.
- An aesthetic.

Do you notice fonts all around you- on neon signs, and restaurant awnings?\
Do you notice colors?\
Do you _feel_/_taste_?\
Do you pause to appreciate a shot in a movie that no one else cares about?


Your edge is often at what you notice, and the world ignores.

## Be real.
If you are truly authentic, different is natural.
- Your 'brand' is a true extension of you. 
- Your 'values' are the brands' throughlines.
- Your 'content' is the world that you alone define and share.
